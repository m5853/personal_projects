new Swiper('.slider', {
    // direction: 'vertical',
    speed: 2400,
    mousewheel: {
        enabled: true,
        sensitivity: 2.4,
    },
    spaceBetween: 10,
    parallax: true,
    freeMode: true,
})