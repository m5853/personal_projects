import React from "react";
import {Button} from "../Button/Button";
import styles from './TodoPanel.module.css';

//Начальное значение поля ввода
const todo_def = {
    note: ''
}

//Интерфейс для добавления заметки
interface AddTodoProps {
    mode:'add';
    addTodo:({note}: Omit<Todo, 'checked' | 'id'>) => void;
}

//Интерфейс для исправления заметки
interface EditTodoProps {
    mode:'edit';
    editTodo: Omit<Todo, 'id' | 'checked'>;
    changeTodo:({note}: Omit<Todo, 'checked' | 'id'>) => void;
}

export const TodoPanel:React.FC<TodoPanelProps> = (props) => {
    //Проверка состояния панели ввода
    const isEdit = props.mode === 'edit';
    const [todo, setTodo] = React.useState(isEdit ? props.editTodo : todo_def);

    //Отслеживания символов вводимых в input
    const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const {name, value} = event.target;
        setTodo({...todo, [name]: value});
    };

    //
    const onClick = () => {
        const todoItem = {note: todo.note}
        if(isEdit) {
            return props.changeTodo(todoItem)
        }
        props.addTodo(todoItem)
        setTodo(todo_def)
    }

    return <div className={styles.todo_panel}>
        <input className={styles.panel_input} type="text" id="note" name="note" value={todo.note} placeholder={"Введите текст"} onChange={onChange}/>
        {!isEdit && (<Button color={'blue'} onClick={onClick}>Добавить</Button>)}
        {isEdit && (<Button color={'orange'} onClick={onClick}>Изменить</Button>)}

    </div>
}