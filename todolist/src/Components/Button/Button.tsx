import React from "react";
import styles from './Button.module.css';

//Определение того, какие параметры кнопка будет принимать на вход
interface ButtonProps extends React.ComponentPropsWithRef<'button'> {
    color: 'blue' | 'orange' | 'red';
}

export const Button:React.FC<ButtonProps> = ({children, color, onClick, ...props}) => {

    const className = `${styles.button} ${styles[`button_${color}`]}`

    return (
        <button className={className} onClick={onClick} {...props}>
            {children}
        </button>
    );
};