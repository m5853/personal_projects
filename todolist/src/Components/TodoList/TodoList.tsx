import React from "react";
import {TodoItem} from "./Todoitem/TodoItem";
import {TodoPanel} from "../TodoPanel/TodoPanel";

interface TodoListProps {
    todos: Todo[];
    toggleTodo: ToggleTodo;
    deleteTodo: DeleteTodo;
    selectTodoForEdit: SelectTodoForEdit;
    todoEdit: TodoEdit;
    changeTodo: ChangeTodo;
}

export const TodoList: React.FC<TodoListProps> = ({todos, toggleTodo, deleteTodo, selectTodoForEdit, todoEdit, changeTodo}) => (
    <div>
        {todos.map((todo) => {
            if(todo.id === todoEdit) return <TodoPanel key={(todo.id)} mode='edit' editTodo={{name: todo.note}} changeTodo={changeTodo}/>;

            return (
                <TodoItem key={todo.id} todo={todo} toggleTodo={toggleTodo} deleteTodo={deleteTodo} selectTodoForEdit={selectTodoForEdit} />
            );
        })};
    </div>
);

