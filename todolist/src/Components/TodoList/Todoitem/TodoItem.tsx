import React from "react";
import {Button} from "../../Button/Button";
import styles from './TodoItem.module.css';

interface TodoItemProps {
    todo: Todo;
    toggleTodo: ToggleTodo;
    deleteTodo: DeleteTodo;
    selectTodoForEdit: SelectTodoForEdit;
}

export const TodoItem: React.FC<TodoItemProps> = ({todo,toggleTodo, deleteTodo, selectTodoForEdit}) => {

    return <div className={styles.container__todo_item}>
        <div className={styles.item_text}>
            <input
            type="checkbox"
            checked={todo.checked}
            onClick={() => {
                toggleTodo(todo);
            }}/>
            {todo.note}
        </div>
        <div className={styles.item_button}>
        <Button color={'orange'} onClick={() => selectTodoForEdit(todo.id)}>Изменить</Button>
        <Button color={'red'} onClick={() => deleteTodo(todo.id)}>Удалить</Button>
        </div>
        </div>
};