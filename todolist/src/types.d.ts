type Todo = {
    id: number;
    note: string;
    checked: boolean;
};

type ToggleTodo = (selectedTodo: Todo) => void;
type DeleteTodo = (id: Todo['id']) => void;
type SelectTodoForEdit = (id: Todo['id']) => void;
type TodoEdit = Todo['id'] | null;
type TodoPanelProps = AddTodoProps | EditTodoProps;
type ChangeTodo = ({note}: Omit<Todo, 'checked' | 'id'>) => void;