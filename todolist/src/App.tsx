import React from 'react';
import './App.css';
import {TodoPanel} from "./Components/TodoPanel/TodoPanel";
import {TodoList} from "./Components/TodoList/TodoList";

//Список для запсии поледующих заметок c начальными эл-ми
const initialTodos = [
  {
      id: 1,
      note: 'Погулять с собакой',
      checked: false,
  },
  {
      id:2,
      note: 'Изучить React',
      checked: true,
 },
];

export const App = () => {
  const [todos, setTodos] = React.useState(initialTodos)

  const [todoEdit, setTodoEdit] =React.useState<Todo['id'] | null>(null);

  const selectTodoForEdit = (id: Todo['id']) => {
      setTodoEdit(id);
  }

    const addTodo = ({note}: Omit<Todo, 'checked' | 'id'>) => {
      setTodos([...todos, {id: todos[todos.length - 1].id+1, note , checked:false }]);
  };

    const toggleTodo: ToggleTodo = (selectedTodo: Todo) => {
        const newTodos = todos.map((todo) => {
            if (todo === selectedTodo) {
                return {
                    ...todo,
                    checked: !todo.checked,
                };
            }
            return todo;
        });
        setTodos(newTodos);
    };


    const deleteTodo = (id: Todo['id']) => {
        setTodos(todos.filter((todo) => todo.id != id));
    };

    const changeTodo = ({note}: Omit<Todo, 'checked' | 'id'>) => {
        setTodos(
            todos.map((todo) => {
                if (todo.id === todoEdit){
                    return {...todo, note};
                }

                return todo;
            })
        );
        setTodoEdit(null)
    };

  return (
      <div className={'blur'}>
        <div className={'container'}>
          <h1 className={'page-title'}>Список заданий</h1>
            <div >
            <TodoPanel addTodo={addTodo} mode='add'/>
            </div>
            <div >
            <TodoList todos={todos} toggleTodo={toggleTodo} deleteTodo={deleteTodo} selectTodoForEdit={selectTodoForEdit} todoEdit={todoEdit} changeTodo={changeTodo}/>
            </div>
            </div>
      </div>
  );
};