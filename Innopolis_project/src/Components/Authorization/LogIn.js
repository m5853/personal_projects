import './LogIn.css';
import '../Global.css';
import {useEffect, useState} from "react";
import {Link} from "react-router-dom";

export default function LogIn(){

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [emailFull, setEmailFull] = useState(false);
    const [passwordFull, setPasswordFull] = useState(false);

    const [emailError, setEmailError] = useState('Поле должно быть заполнено');
    const [passwordError, setPasswordError] = useState('Поле должно быть заполнено');

    const [formValid, setFormValid] = useState(false);

    const blureHandler = (e) => {
        switch (e.target.type) {
            case 'email': setEmailFull(true); break;
            case 'password': setPasswordFull(true); break;
        }
    }

    const emailHendler = (e) => {
        setEmail(e.target.value);
        const re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (!re.test(String(e.target.value).toLowerCase())) {
             setEmailError('Введен некорректный email');
        }else {
            setEmailError('');
        }
    }

    const passwordHendler = (e) => {
        setPassword(e.target.value);
        const re = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
        if (!re.test(String(e.target.value))) {
            setPasswordError('Введен некорректный gfhjjn');
        }else {
            setPasswordError('');
        }
    }

    useEffect( () => {
        if (emailError || passwordError) {
            setFormValid(false);
        }else {
            setFormValid(true);
        }

    }, [emailError, passwordError])

    return(
        <div className={'container'}>
            <h2 className="page-title">Вход</h2>

            <form className={'form'}>
                <div className={'form__container'}>

                <input className="form__input-email form__input_theme" type={'email'} onBlur={e => blureHandler(e)} onChange={e => emailHendler(e)} id={"input-email"}
                       placeholder={'Введите Email'}/>
                    {(emailFull && emailError) && <div style={{color: 'red'}}>{emailError}</div>}

                <input className="form__input-password form__input_theme" type={'password'} onBlur={e => blureHandler(e)} onChange={e => passwordHendler(e)} id="input-password"
                       placeholder="Введите пароль"/>
                    {(passwordFull && passwordError) && <div style={{color: 'red'}}>{passwordError}</div>}


                    <div className="from__checkbox-wrapper">
                    <input className="form__checkbox" type="checkbox" id="input-checkbox"/>
                        <div className="form__checkbox-mark"></div>
                        <label className="form__checkbox-lable" htmlFor="input-checkbox">Я согласен получать обновления
                            на почту</label>
                </div>

                    <p>У вас еше нет аккаунта? - <Link to={'/register'} className={'register-link'} >Пора зарегистрироваться</Link></p>

                    <Link to={'/'} onClick={!formValid}> <button className="btn">Войти</button></Link>

                </div>

            </form>
        </div>

    )
};