import '../Global.css';
import './Advantages.css';

export default function Advantages () {
    return (
        <section id="advantages" className="advantages section-offset">
            <div className="container">
                <h2 className="section-title advantages-title">
                    Преимущества
                </h2>
                <ul className="list-resert advantages-list flex">
                    <li className="advantages-item">
                        <p className="advantages-item-text advantages-item-img advantages-item-img-1">
                            Идейные соображения высшего порядка, а&nbsp;также постоянный количественный рост
                        </p>
                    </li>

                    <li className="advantages-item ">
                        <p className="advantages-item-text advantages-item-img advantages-item-img-2">
                            Значимость этих проблем настолько очевидна, что вопрос остается открытым
                        </p>

                    </li>

                    <li className="advantages-item">
                        <p className="advantages-item-text advantages-item-img advantages-item-img-3">
                            Таким образом реализация плановых заданий играет важную роль для понимания
                        </p>
                    </li>

                    <li className="advantages-item">
                        <p className="advantages-item-text advantages-item-img advantages-item-img-4">
                            Повседневная практика показывает, что сложившаяся структура организации
                        </p>
                    </li>

                    <li className="advantages-item">
                        <p className="advantages-item-text advantages-item-img advantages-item-img-5">
                            Равным образом рамки и&nbsp;место обучения кадров способствует подготовки сотрудника
                        </p>
                    </li>
                    <li className="advantages-item">
                        <p className="advantages-item-text advantages-item-img advantages-item-img-6">
                            Консультация с&nbsp;активом влечет за&nbsp;собой процесс внедрения услуг нашего сервиса
                        </p>
                    </li>

                    <li className="advantages-item">
                        <p className="advantages-item-text advantages-item-img advantages-item-img-7">
                            Повседневная практика показывает, что дальнейшее развитие различных форм
                        </p>
                    </li>

                    <li className="advantages-item">
                        <p className="advantages-item-text advantages-item-img advantages-item-img-8">
                            Значимость этого настолько очевидна, что консультация наших экспертов помогает
                        </p>
                    </li>

                </ul>
            </div>
        </section>

    )
}