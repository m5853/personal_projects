import '../Global.css';
import './IndexPage.css';

export default function IndexPage(){
    return(
        <section className="special-offer section-offset">
            <div className="container">
                <h2 className="section-title special-offer-title">
                    У нас вы можете узнать
                </h2>
                <div className="special-offer-card flex">
                    <div className="special-offer-card-img ">
                        <div className="card card-1 flex">
                            <h3 className="special-offer-card-title">Мальдивские острова</h3>
                            <p className="special-offer-price">от 55 000 р</p>
                        </div>
                        <div className="card card-2 flex">
                            <h3 className="special-offer-card-title">Горящий тур на остров Крит</h3>
                            <p className="special-offer-price">от 30 000 р</p>
                        </div>
                    </div>
                    <div className="special-offer-card-img ">
                        <div className="card card-3 flex">
                            <h3 className="special-offer-card-title-2">Номера категории люкс</h3>
                            <p className="special-offer-price-2">от 5 000 р</p>
                        </div>
                    </div>
                </div>

                <p className="special-offer-descr">Воспользуйтесь нашей панелью навигации, чтобы узнать больше о нас, наших возможностях и преимуществах.</p>

            </div>

        </section>
    )
}