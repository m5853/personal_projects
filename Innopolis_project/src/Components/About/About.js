import '../Global.css';
import './About.css';

export default function About () {
    return (
        <section id="about-us" className="about-us section-offset">
            <div className="container">
                <h2 className="section-title about-us-title">
                    О нас
                </h2>
                <p className="about-us-descr">
                    Идейные соображения высшего порядка, а&nbsp;также сложившаяся структура организации влечет
                    за&nbsp;собой
                    процесс внедрения и&nbsp;модернизации системы обучения кадров, соответствует насущным потребностям.
                    Идейные
                    соображения высшего порядка, а&nbsp;также дальнейшее развитие различных форм деятельности
                    представляет собой
                    интересный эксперимент проверки системы обучения кадров, соответствует насущным потребностям. Равным
                    образом
                    сложившаяся структура организации требуют определения и&nbsp;уточнения существенных финансовых
                    и&nbsp;административных условий.
                </p>

            </div>

            <div className="container">
                <h2 className="section-title contacts-title">Контакты</h2>
                <div className="contacts-zone flex ">
                    <div className="contacts-left">
                        <address className="contacts-address">
                            <ul className="list-resert contacts-list">
                                <li className="contacts-item flex">
                                    <div className="contacts-item-title ">Адрес</div>
                                    <p className="contacts-item-descr address-descr">Москва, улица Юности, дом
                                        5&nbsp;строение&nbsp;4, офис&nbsp;2</p>
                                </li>
                                <li className="contacts-item flex">
                                    <div className="contacts-item-title">Телефоны</div>
                                    <div className="contacts-item-descr flex">
                                        <a className="link" href="#">+7 (499) 535-64-34</a>
                                        <a className="link" href="#">+7 (495) 005-05-44</a>
                                    </div>
                                </li>
                                <li className="contacts-item flex">
                                    <div className="contacts-item-title">E-mail</div>
                                    <div className="contacts-item-descr flex">
                                        <a className="link" href="#">lg.oona@mail.ru</a>
                                        <span className="link-descr">по вопросам бронирования</span>
                                        <a className="link" href="#">hotels.ln@mail.ru</a>
                                        <span className="link-descr">по вопросам сотрудничества</span>
                                    </div>
                                </li>
                                <li className="contacts-item flex">
                                    <div className="contacts-item-title">График</div>
                                    <p className="contacts-item-descr address-descr">Понедельник&nbsp;&mdash; пятница,
                                        с&nbsp;10:00 до&nbsp;19:00a</p>

                                </li>
                            </ul>

                        </address>

                        <button className="btn btn-resert btn-contacts">Построить маршрут</button>


                    </div>
                    <div className="contacts-right">

                    </div>
                </div>


            </div>
        </section>

    )
}