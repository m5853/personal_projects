import React from 'react';
import { useSelector} from 'react-redux';
import { OrderItem } from '../Order-item/Oreder-item';
import './order-page.css';
import {createSlice} from "@reduxjs/toolkit";

export const FavoritePage = () => {
    const cartSlice = createSlice({
        name: 'cart',
        initialState: {
            itemsInCart: [],
        },
        reducers: {
            setItemInCart: (state, action) => {
                state.itemsInCart.push(action.payload)
            },
            deleteItemFromCart: (state, action) => {
                state.itemsInCart = state.itemsInCart.filter(game => game.id !== action.payload)
            },
        }
    });
    const items = useSelector((state) => state.cart.itemsInCart);

    if(items.length < 1) {
        return <h1>Ваша корзина пуста!</h1>
    }

    return (
        <div className="order-page">
            <div className="order-page__left">
                { items.map(game => <OrderItem game={game}/>)}
            </div>
        </div>
    )
}