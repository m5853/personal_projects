import React from "react";
import Placement from '../Placement/Placement';
import './PlacementList.css';

export default class PlacementList extends React.Component {
    static defaultProps = {
        locationList: []
    }

    render() {

        return (
            <div className={'advantages section-offset'}>
                <div className="container">
                {
                    this.props.locationList.map((loc) => {

                        return (
                            <div onClick={(loc) => null} key={loc.id}>
                                <Placement label={loc.label} locationName={loc.locationName} lat={loc.location.lat} lon={loc.location.lon}/>
                            </div>
                        )
                    })
                }
                </div>
            </div>
    )};
}
