import React from "react";
import '../Global.css';
import '../Placement/Placement.css'

export default class Placement extends React.Component {
    static defaultProps = {
        id:0,
        label: '',
        locationName: '',
        lat: 0,
        lon: 0
    }

render() {
    const {label, locationName, lat, lon} = this.props;

    return (
        <div className={"placement-item"}>
            <h2>{label}</h2>
            <p>{locationName}</p>
            <p>Широта: {lat}  Долгота: {lon}</p>

        </div>
    )
}
}