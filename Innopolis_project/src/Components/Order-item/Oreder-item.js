import React from 'react';
import { useDispatch } from 'react-redux';
import './order-item.css';
import { deleteItemFromCart } from '../Favorite/Cart/reducer';

export const OrderItem = ({ game }) => {
    const dispatch = useDispatch();
    const handleDeleteClick = () => {
        dispatch(deleteItemFromCart(game.id))
    }
    return (
        <div className="order-item">
            <div className="order-item__cover">
                {/*<GameCover image={ game.image }/>*/}
            </div>
            <div className="order-item__title">
                <span> { game.title } </span>
            </div>
            <div className="order-item__price">
                <span>{ game.price } руб.</span>
                <div
                    size={25}
                    className="cart-item__delete-icon"
                    onClick={handleDeleteClick}
                >X</div>
            </div>
        </div>
    )
}