import './Header.css';
import '../Global.css';
import logo from '../../img/logo.svg';
import {Link} from "react-router-dom";

export default function Header() {
    return (
        <header className="header">
            <div className="container header-container">
                <div className="header-up flex">
                    <div className="header-left flex">
                        <Link to={'/'} className="header-logo"> <img src={logo} alt="Logo"/></Link>
                        <address className="header-contact">
                            <a href="tel:+74953225448" className="header-contact-link">
                                +7 495 322 54 48
                            </a>
                        </address>
                    </div>

                    <div className="header-right flex">
                        <Link to={'LogIn'} className="header-account">Личный кабинет</Link>
                    </div>
                </div>

                <div className="header-down flex">
                    <div className="header-left header-left-p flex">
                        <nav className="header-nav">
                            <ul className="list-resert header-list flex">
                                <li className="header-list-item">
                                    <Link to={'about'} className="header-link">О нас</Link>
                                </li>
                                <li className="header-list-item">
                                    <Link to={'advantages'} className="header-link">Преимущества</Link>
                                </li>
                                <li className="header-list-item">
                                    <Link to={'placement'} className="header-link">Отели</Link>

                                </li>
                                <li className="header-list-item">
                                    <Link to={'reviews'} className="header-link">Отзывы</Link>

                                </li>

                            </ul>
                        </nav>

                    </div>

                    <div className="header-right flex">
                        <div className="header-btn-position">
                            <button className="btn btn-resert header-btn">
                                Избранное
                            </button>
                            <button className="btn btn-resert header-btn">
                                Обратный звонок
                            </button>
                        </div>
                    </div>

                </div>

            </div>
        </header>
    )
}