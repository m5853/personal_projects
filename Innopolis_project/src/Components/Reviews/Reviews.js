import {useEffect, useState} from "react";
import '../Global.css';
import './Reviews.css';

export default function Reviews (){
    const [userInput, setUserInput] = useState('');
    const [reviews, setReviews] = useState([]);

    const addReview = (userInput) => {
        if (userInput) {
            const newItem = {
                id: Math.random().toString(36).substr(2,3),
                review: userInput,
            }
            setReviews([...reviews, newItem])
        }

    }

    const deleteReview = (id) => {
        setReviews([...reviews.filter((review) => review.id !== id)])
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        addReview(userInput)

        setUserInput('')
    }
    const handleChange = (e) => {
        setUserInput(e.currentTarget.value)
    }

    return(
        <div className={'container'}>
            <h2 className={'section-title'}>Список отзывов </h2>
            <form className={'form-review'} onSubmit={handleSubmit} addReview={addReview}>
                <textarea className={'textarea-review'} value={userInput}  onChange={handleChange} />
                <button className={'btn'}>Сохранить</button>
            </form>
            {reviews.map((review) =>{
                return(
                    <div className={'review-zone'} key={review.id}>
                        <div className={'review-descr'}>
                            {review.review}
                        </div>
                        <div className={'review-descr delReview'} onClick={() => deleteReview(review.id)}>
                            X
                        </div>
                    </div>
                )
            })}

        </div>
    )
}
