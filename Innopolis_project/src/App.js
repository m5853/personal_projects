import './App.css';
import Header from "./Components/Header/Header";
import Footer from "./Components/Footer/Footer";
import {Outlet, Route, Routes} from "react-router-dom";
import IndexPage from "./Components/IndexPage/IndexPage";
import About from "./Components/About/About";
import Advantages from "./Components/Advantages/Advantages";
import PlacementList from "./Components/PlacementList/PlacementList";
import React from "react";
import LogIn from "./Components/Authorization/LogIn";
import Register from "./Components/Authorization/Register";
import Reviews from "./Components/Reviews/Reviews";

export default class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            hotels: []
        };
    }
    componentDidMount(){
        fetch('http://engine.hotellook.com/api/v2/lookup.json?query=all&lang=ru&lookFor=both&limit=10')
            .then((response) => {
                return response.json();
            })
            .then((body) => {
                this.setState({hotels: body.results.hotels});
            });

    };

  render () {
      return (
              <Routes>
                  <Route path={'/'} element={<Layout/>}>
                      <Route index element={<IndexPage/>}/>
                      <Route path={'about'} element={<About/>}/>
                      <Route path={'advantages'} element={<Advantages/>}/>
                      <Route path={'placement'} element={<PlacementList locationList = {this.state.hotels}/>}/>
                      <Route path={'login'} element={<LogIn/>}/>
                      <Route path={'register'} element={<Register/>}/>
                      <Route path={'reviews'} element={<Reviews/>}/>
                  </Route>
              </Routes>
  ) };
}

function Layout(){
    return(
        <div className="App">
          <Header/>
          <main >
              <Outlet/> 
          </main>
          <Footer/>
        </div>
    )
}


