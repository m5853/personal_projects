import { Component, OnInit } from '@angular/core';
import { Note } from '../models/note.model';
import { NotesService } from '../service/notes.service';

@Component({
  selector: 'app-notes-page',
  templateUrl: './notes-page.component.html',
  styleUrls: ['./notes-page.component.css']
})
export class NotesPageComponent implements OnInit {

  notes: Note[] = [];
  note: Note = {
    id: '',
    title: '',
    dateCreated: '',
    text: ''
  }

  constructor(private notesService: NotesService) {

  }

  ngOnInit(): void {
    this.getAllNotes();
  }

  getAllNotes () {
    this.notesService.getAllNotes()
    .subscribe(
      response => {
        this.notes = response
      }
    )
  }

  onSubmit() {
    this.notesService.addNote(this.note)
    .subscribe(
      response => {
        this.getAllNotes();
        this.note = {
          id: '',
          title: '',
          dateCreated: '',
          text: ''
        }
      }
    )
  }

  deleteNote(id: string) {
    this.notesService.deleteNote(id)
    .subscribe(
      response => {
        this.getAllNotes();
      }
    )

  }

}
