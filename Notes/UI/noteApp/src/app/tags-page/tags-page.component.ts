import { Component, OnInit } from '@angular/core';
import { Tag } from '../models/tag.model';
import { TagsService } from '../service/tags.service';

@Component({
  selector: 'app-tags-page',
  templateUrl: './tags-page.component.html',
  styleUrls: ['./tags-page.component.css']
})

export class TagsPageComponent implements OnInit {

  tags: Tag[] = [];
  tag: Tag = {
    id: '',
    tagName: '',
    color: ''
  }

  constructor(private tagsService: TagsService) {

  }

  ngOnInit(): void {
    this.getAllTags();
  }

  getAllTags () {
    this.tagsService.getAllTags()
    .subscribe(
      response => {
        this.tags = response
      }
    )
  }

  onSubmit() {
    this.tagsService.addTag(this.tag)
    .subscribe(
      response => {
        this.getAllTags();
        this.tag = {
          id: '',
          tagName: '',
          color: ''
        }
      }
    )
  }

  deleteTag(id: string) {
    this.tagsService.deleteTag(id)
    .subscribe(
      response => {
        this.getAllTags();
      }
    )

  }

}

