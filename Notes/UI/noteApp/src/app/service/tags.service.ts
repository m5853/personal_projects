import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Tag } from '../models/tag.model';

@Injectable({
  providedIn: 'root'
})
export class TagsService {

  baseUrl = 'https://localhost:7200/api/tags'

  constructor(private http: HttpClient) { }


  //Get all tags
  getAllTags(): Observable<Tag[]> {
    return this.http.get<Tag[]>(this.baseUrl);
  }

  addTag(tag: Tag): Observable<Tag> {
    tag.id = '00000000-0000-0000-0000-000000000000';
    return this.http.post<Tag>(this.baseUrl, tag);
  }

  deleteTag(id: string): Observable<Tag> {
    return this.http.delete<Tag>(this.baseUrl + '/' + id);
  }
}
