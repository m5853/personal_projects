import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Reminder } from '../models/reminder.model';

@Injectable({
  providedIn: 'root'
})

export class RemindersService {

  baseUrl = 'https://localhost:7200/api/reminders'

  constructor(private http: HttpClient) { }


  //Get all notes
  getAllReminders(): Observable<Reminder[]> {
    return this.http.get<Reminder[]>(this.baseUrl);
  }

  addReminder(reminder: Reminder): Observable<Reminder> {
    reminder.id = '00000000-0000-0000-0000-000000000000';
    return this.http.post<Reminder>(this.baseUrl, reminder);
  }

  deleteReminder(id: string): Observable<Reminder> {
    return this.http.delete<Reminder>(this.baseUrl + '/' + id);
  }
}
