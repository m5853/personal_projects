import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Note } from '../models/note.model';

@Injectable({
  providedIn: 'root'
})

export class NotesService {

  baseUrl = 'https://localhost:7200/api/notes'

  constructor(private http: HttpClient) { }


  //Get all notes
  getAllNotes(): Observable<Note[]> {
    return this.http.get<Note[]>(this.baseUrl);
  }

  addNote(note: Note): Observable<Note> {
    let date = new Date().toISOString();
    note.id = '00000000-0000-0000-0000-000000000000';
    note.dateCreated = date.toString();
    console.log(date);
    return this.http.post<Note>(this.baseUrl, note);
  }

  deleteNote(id: string): Observable<Note> {
    return this.http.delete<Note>(this.baseUrl + '/' + id);
  }
}
