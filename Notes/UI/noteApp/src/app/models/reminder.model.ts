export interface Reminder {
  id: string;
  title: string;
  text: string;
  date: string;
  time:string;
}
