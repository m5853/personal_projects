export interface Note {
  id: string;
  title: string;
  dateCreated: string;
  text: string
}
