import { Component, OnInit } from '@angular/core';
import { Reminder } from '../models/reminder.model';
import { RemindersService } from '../service/reminders.service';

@Component({
  selector: 'app-reminders-page',
  templateUrl: './reminders-page.component.html',
  styleUrls: ['./reminders-page.component.css']
})
export class RemindersPageComponent implements OnInit {

  reminders: Reminder[] = [];
  reminder: Reminder = {
    id: '',
    title: '',
    text: '',
    date: '',
    time: ''
  }

  constructor(private remindersService: RemindersService) {

  }

  ngOnInit(): void {
    this.getAllReminders();
  }

  getAllReminders() {
    this.remindersService.getAllReminders()
      .subscribe(
        response => {
          this.reminders = response
        }
      )
  }

  onSubmit() {
    this.remindersService.addReminder(this.reminder)
      .subscribe(
        response => {
          this.getAllReminders();
          this.reminder = {
            id: '',
            title: '',
            text: '',
            date: '',
            time: ''
          }
        }
      )
  }

  deleteReminder(id: string) {
    this.remindersService.deleteReminder(id)
      .subscribe(
        response => {
          this.getAllReminders();
        }
      )

  }

}
