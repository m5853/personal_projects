import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HomePageComponent } from './home-page/home-page.component';
import { RouterModule } from '@angular/router';
import { TagsPageComponent } from './tags-page/tags-page.component';
import { NotesPageComponent } from './notes-page/notes-page.component';
import { RemindersPageComponent } from './reminders-page/reminders-page.component';

const routes = [
  {path: '', component: HomePageComponent},
  {path: 'notes', component: NotesPageComponent},
  {path: 'reminders', component: RemindersPageComponent},
  {path: 'tags', component: TagsPageComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    TagsPageComponent,
    NotesPageComponent,
    RemindersPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
