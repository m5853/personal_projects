using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using NotesApi.Data;
using NotesApi.Models;

namespace NotesApi.Controllers;

[ApiController]
[Route("api/[controller]")]
public class TagsController : Controller
{
    private readonly MainContext mainDbContext;

    public TagsController(MainContext mainDbContext)
    {
        this.mainDbContext = mainDbContext;
    }

    //Get all tags
    [HttpGet]
    public async Task<IActionResult> GetAllTags()
    {
        var tags = await mainDbContext.Tags.ToListAsync();
        return Ok(tags);
    }

    //Get single tag
    [HttpGet]
    [Route("{id:guid}")]
    [ActionName("GetTag")]
    public async Task<IActionResult> GetTag ([FromRoute] Guid id)
    {
        var tag = await mainDbContext.Tags.FirstOrDefaultAsync(x => x.Id == id);
        if (tag != null)
        {
            return Ok(tag);
        }
        return NotFound("Tag not found");
    }
    
    //Add tag
    [HttpPost]
    public async Task<IActionResult> AddTag([FromBody] Tag tag)
    {
        tag.Id = Guid.NewGuid();
        await mainDbContext.Tags.AddAsync(tag);
        await mainDbContext.SaveChangesAsync();
        return CreatedAtAction(nameof(GetTag), new {id = tag.Id}, tag);
    }
    
    //Update Tag
    [HttpPut]
    [Route("{id:guid}")]
    public async Task<IActionResult> UpdateTag([FromRoute] Guid id, [FromBody] Tag tag)
    {
        var existingTag = await mainDbContext.Tags.FirstOrDefaultAsync(x => x.Id == id);
        if (existingTag != null)
        {
            existingTag.TagName = tag.TagName;
            existingTag.Color = tag.Color;
            await mainDbContext.SaveChangesAsync();
            return Ok(existingTag);
        }
        return NotFound("Tag not found");
    }
    
    //Delete tag
    [HttpDelete]
    [Route("{id:guid}")]
    public async Task<IActionResult> DeleteTag([FromRoute] Guid id)
    {
        var existingTag = await mainDbContext.Tags.FirstOrDefaultAsync(x => x.Id == id);
        if (existingTag != null)
        {
            mainDbContext.Remove(existingTag);
            await mainDbContext.SaveChangesAsync();
            return Ok(existingTag);
        }
        return NotFound("Tag not found");
    }
}