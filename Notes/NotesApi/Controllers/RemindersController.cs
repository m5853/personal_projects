﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NotesApi.Data;
using NotesApi.Models;

namespace NotesApi.Controllers;

[ApiController]
[Route("api/[controller]")]
public class RemindersController: Controller
{
    private readonly MainContext mainDbContext;

    public RemindersController(MainContext mainDbContext)
    {
        this.mainDbContext = mainDbContext;
    }

    //Get all notes
    [HttpGet]
    public async Task<IActionResult> GetAllReminders()
    {
        var reminders = await mainDbContext.Reminders.ToListAsync();
        return Ok(reminders);
    }

    //Get single note
    [HttpGet]
    [Route("{id:guid}")]
    [ActionName("GetReminder")]
    public async Task<IActionResult> GetReminder ([FromRoute] Guid id)
    {
        var reminder = await mainDbContext.Reminders.FirstOrDefaultAsync(x => x.Id == id);
        if (reminder != null)
        {
            return Ok(reminder);
        }
        return NotFound("Tag not found");
    }
    
    //Add tag
    [HttpPost]
    public async Task<IActionResult> AddReminder([FromBody] Reminder reminder)
    {
        reminder.Id = Guid.NewGuid();
        await mainDbContext.Reminders.AddAsync(reminder);
        await mainDbContext.SaveChangesAsync();
        return CreatedAtAction(nameof(GetReminder), new {id = reminder.Id}, reminder);
    }
    
    //Update Tag
    [HttpPut]
    [Route("{id:guid}")]
    public async Task<IActionResult> UpdateReminder([FromRoute] Guid id, [FromBody] Reminder reminder)
    {
        var existingReminder = await mainDbContext.Reminders.FirstOrDefaultAsync(x => x.Id == id);
        if (existingReminder != null)
        {
            existingReminder.Title = reminder.Title;
            existingReminder.Text = reminder.Text;
            existingReminder.Date = reminder.Date;
            existingReminder.Time = reminder.Time;
           
            await mainDbContext.SaveChangesAsync();
            return Ok(existingReminder);
        }
        return NotFound("Tag not found");
    }
    
    //Delete tag
    [HttpDelete]
    [Route("{id:guid}")]
    public async Task<IActionResult> DeleteReminder([FromRoute] Guid id)
    {
        var existingReminder = await mainDbContext.Reminders.FirstOrDefaultAsync(x => x.Id == id);
        if (existingReminder != null)
        {
            mainDbContext.Remove(existingReminder);
            await mainDbContext.SaveChangesAsync();
            return Ok(existingReminder);
        }
        return NotFound("Tag not found");
    }
    
}