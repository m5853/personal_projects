﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NotesApi.Data;
using NotesApi.Models;

namespace NotesApi.Controllers;

[ApiController]
[Route("api/[controller]")]
public class NotesController: Controller
{
    private readonly MainContext mainDbContext;

    public NotesController(MainContext mainDbContext)
    {
        this.mainDbContext = mainDbContext;
    }

    //Get all notes
    [HttpGet]
    public async Task<IActionResult> GetAllNotes()
    {
        var notes = await mainDbContext.Notes.ToListAsync();
        return Ok(notes);
    }

    //Get single note
    [HttpGet]
    [Route("{id:guid}")]
    [ActionName("GetNote")]
    public async Task<IActionResult> GetNote ([FromRoute] Guid id)
    {
        var note = await mainDbContext.Notes.FirstOrDefaultAsync(x => x.Id == id);
        if (note != null)
        {
            return Ok(note);
        }
        return NotFound("Tag not found");
    }
    
    //Add tag
    [HttpPost]
    public async Task<IActionResult> AddNote([FromBody] Note note)
    {
        note.Id = Guid.NewGuid();
        await mainDbContext.Notes.AddAsync(note);
        await mainDbContext.SaveChangesAsync();
        return CreatedAtAction(nameof(GetNote), new {id = note.Id}, note);
    }
    
    //Update Tag
    [HttpPut]
    [Route("{id:guid}")]
    public async Task<IActionResult> UpdateNote([FromRoute] Guid id, [FromBody] Note note)
    {
        var existingNote = await mainDbContext.Notes.FirstOrDefaultAsync(x => x.Id == id);
        if (existingNote != null)
        {
            existingNote.Title = note.Title;
            existingNote.DateCreated = note.DateCreated;
            existingNote.Text = note.Text;
            await mainDbContext.SaveChangesAsync();
            return Ok(existingNote);
        }
        return NotFound("Tag not found");
    }
    
    //Delete tag
    [HttpDelete]
    [Route("{id:guid}")]
    public async Task<IActionResult> DeleteNote([FromRoute] Guid id)
    {
        var existingNote = await mainDbContext.Notes.FirstOrDefaultAsync(x => x.Id == id);
        if (existingNote != null)
        {
            mainDbContext.Remove(existingNote);
            await mainDbContext.SaveChangesAsync();
            return Ok(existingNote);
        }
        return NotFound("Tag not found");
    }
}