﻿using System.ComponentModel.DataAnnotations;

namespace NotesApi.Models;

public class Reminder
{
    [Key]
    public Guid Id { get; set; }
    
    public string Date { get; set; }
    public string Time { get; set; }
    public string Title { get; set; }
    public string Text { get; set; }


}