﻿using System.ComponentModel.DataAnnotations;

namespace NotesApi.Models;

public class Tag
{
    [Key]
    public Guid Id { get; set; }
    
    public string TagName { get; set; }
    public string Color { get; set; }
}