﻿using System.ComponentModel.DataAnnotations;

namespace NotesApi.Models;

public class Note
{
    [Key]
    public Guid Id { get; set; }
    
    public string Title { get; set; }
    public DateTime DateCreated { get; set; } = DateTime.Now;
    public string Text { get; set; }
    
}